#!/usr/bin/env python3

import os
import sys
# import time
from threading import Thread
import paho.mqtt.client as mqtt

from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS

from sprut import SprutAPI

sprut = SprutAPI(os.environ.get("SPRUT_USERNAME"), os.environ.get("SPRUT_PASSWORD"), os.environ.get("SPRUT_HUB"))
influx_cli = InfluxDBClient(os.environ.get("INFLUXDB_URL"), os.environ.get("INFLUXDB_TOKEN"),
                            org=os.environ.get("INFLUXDB_ORG"), verify_ssl=False)
influx_bucket = os.environ.get("INFLUXDB_BUCKET")
influx_write_api = influx_cli.write_api(write_options=SYNCHRONOUS)


class SpruthubMQTTClient(mqtt.Client):
    """MQTT Client for the SprutHub"""

    def on_connect(self, client, obj, flags, rc):
        if rc == 0:
            # self.subscribe("/spruthub/accessories/#", 0)
            self.subscribe([
                ("/spruthub/accessories/+/+/BatteryService/ChargingState", 0),
                ("/spruthub/accessories/+/+/BatteryService/BatteryLevel", 0),
                ("/spruthub/accessories/+/+/BatteryService/StatusLowBattery", 0),
                ("/spruthub/accessories/+/+/TemperatureSensor/CurrentTemperature", 0),
                ("/spruthub/accessories/+/+/MotionSensor/MotionDetected", 0),
                ("/spruthub/accessories/+/+/LightSensor/CurrentAmbientLevel", 0),
                ("/spruthub/accessories/+/+/Outlet/On", 0),
                ("/spruthub/accessories/+/+/Outlet/OutletInUse", 0),
                ("/spruthub/accessories/+/+/ContactSensor/ContactSensorState", 0),
                ("/spruthub/accessories/+/+/C_AtmosphericPressureSensor/C_CurrentAtmosphericPressure", 0),
                ("/spruthub/accessories/+/+/HumiditySensor/CurrentRelativeHumidity", 0),
                ("/spruthub/accessories/+/+/LeakSensor/LeakDetected", 0),
                ("/spruthub/accessories/+/+/Lightbulb/On", 0),
                ("/spruthub/accessories/+/+/Lightbulb/Brightness", 0),
                ("/spruthub/accessories/+/+/Switch/On", 0),
                ("/spruthub/accessories/+/+/SmokeSensor/SmokeDetected", 0)
            ])
            print(f'SPRUT MQTT > Subscribed!')

    def on_message(self, client, obj, msg):
        if msg.topic != '/spruthub/system/log':
            print(f'SPRUT MQTT > {msg.topic} = {msg.payload}')
            accessory = msg.topic.split('/')
            print(accessory)
            accessory_id = int(accessory[3])
            service_id = int(accessory[4])
            service_name = accessory[5]
            value_name = accessory[6]
            value_value = msg.payload.decode('utf-8')
            if (accessory_id > 3) and (accessory_id != 39) and (value_value is not None):  # we do not need to parse system accessories
                if (service_id not in [1, 8]) and (value_name != 'Name'):  # services with id = [1, 8] are system
                    accessory = sprut.get_accessory_by_id(accessory_id)
                    if accessory is None:  # it seems like an accessory were added after an api was requested...
                        sprut.load_accessories()  # ...so, reloading accessories from the sprut
                        accessory = sprut.get_accessory_by_id(accessory_id)

                    svc = sprut.get_service_by_id(accessory_id, service_id)  # get service information
                    cr = sprut.get_characteristic_by_name(accessory_id, service_id, value_name)
                    fmt = cr['format'].lower()
                    val = None

                    # print(accessory)
                    # print(fmt)

                    if fmt == 'uint8' or fmt == 'int':
                        val = int(value_value)
                    elif fmt == 'bool':
                        val = bool(value_value)
                    elif fmt == 'float':
                        val = float(value_value)

                    p = Point(accessory['roomName']) \
                          .tag('accessory', accessory['name']) \
                          .tag('deviceId', accessory['serial']) \
                          .tag('service', svc['name']) \
                          .field(value_name, val)
                    w = influx_write_api.write(bucket=influx_bucket, record=p)
                    # print(p.__dict__)
                    # print(w)


def main():
    print(f'Starting a Marriage between Sprut & InfluxDB')

    sprut.auth()
    sprut.load_accessories()

    # while True:
    #     sprut.load_accessories()
    #     print(sprut.accessories)
    #     time.sleep(10)

    sprut_mqtt = SpruthubMQTTClient(client_id="py-sprut/0.1")
    sprut_mqtt.connect(os.environ.get("SPRUT_HUB"), int(os.environ.get("SPRUT_MQTT_PORT")), 60)  # TODO: str port check

    try:
        sprut_mqtt_thread = Thread(target=sprut_mqtt.loop_forever)
        # sprut_mqtt_thread.daemon = True
        sprut_mqtt_thread.start()
        sprut_mqtt_thread.join()
    except (KeyboardInterrupt, SystemExit):
        print('Keyboard interrupt or SystemExit, quitting MQTT threads & Closing DB Connections.')
        sprut_mqtt.disconnect()
        influx_write_api.close()
        influx_cli.close()
        sys.exit()


if __name__ == '__main__':
    main()
