import requests
from datetime import datetime


class Graphite:
    """Graphite connection and metrics class"""
    def __init__(self, username, apikey, url):
        self.username = username  # grafana cloud username for graphite
        self.apikey = apikey      # api key should have editor rights
        self.url = url            # a url for the graphite instance

    def push_metric(self, metric_name, metric_value):
        """Push a metric to the Graphite"""
        timestamp = datetime.now().timestamp()  # current time as a timestamp
        data = [{  # metric data
            'name': metric_name,     # graphite style name (required). should be same as Metric field below
            # 'metric': metric_name,   # in graphite style format. should be same as Name field (used to generate Id)
            'value': metric_value,   # float64 value (required)
            'interval': 1,           # the resolution of the metric in seconds (required)
            # 'unit': '',              # not needed or used yet
            'time': int(timestamp),  # unix timestamp in seconds (required)
            # 'mtype': 'count',        # not used yet. but should be one of gauge, rate, count, counter, timestamp
            'tags': []               # list of key=value pairs or single words of tags (optional)
        }]

        try:
            response = requests.post(
                url=self.url,
                headers={'Authorization': f'Bearer {self.username}:{self.apikey}', 'Content-Type': 'application/json'},
                json=data,
                # auth=(self.username, self.apikey)
            )
            if response.status_code == 200:
                print(f'GRAPHITE > Metric {metric_name} with value {metric_value} sent to cloud successfully')
            else:
                print(f'GRAPHITE > Send metric {metric_name} failed with HTTP code {response.status_code}')
        except requests.exceptions.RequestException as e:
            print(f'GRAPHITE > HTTP Request Error: {e}')
