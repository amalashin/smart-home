**= Smart Home Project Repository =**

[[_TOC_]]

# Подготовка Raspberry Pi
1. Приготовить SD-карту для записи образа
2. Последовательно запустить скрипты (для каждой ноды повторять шаги 2-4):
    - `01.pre.download64.sh` - скачивание и распаковка образа ОС
    - `02.pre.distro64.sh` - запись и очистка дистрибутива
    - `03.cfg.XXX.sh` - настройка и запись конфигурации на SD-карту, где **ХХХ** - имя хоста
        (_saffron_ - мастер-нода, _cardamom_/_turmeric_ - воркер-ноды, _curry_ - сервисная нода)
    - `04.post.eject.sh` - очистка и извлечение готового для запуска диска (если продолжать => не удалять временную директорию)
3. Установить SD-карту в RaspberryPi, подключить кабель Ethernet и подать питание
4. Подождать, пока запустится ОС и установится весь софт (можно подключиться по SSH, на экране будет сообщение 
   _"Automated setup is in progress. When completed, the system will be rebooted."_)

# Мастер-нода & NFS
1. Проверка статуса сервера Kubernetes:
   - `systemctl status k3s`
   - `journalctl -u k3s`
2. Получить ключ для других нод для подключения к кластеру: `cat /var/lib/rancher/k3s/server/node-token`
3. Настройка NFS
   - просмотр доступных блочных устройств: `lsblk`
   - просмотр идентификатора диска: `blkid` (нужен **PARTUUID=xxxx**)
   - в файле `/etc/fstab` добавить строчку: `PARTUUID=xxxx /data ext4 noatime,lazytime,rw,nofail,auto,x-systemd.automount`
   - примортировать диск: `mount /data`
   - создать папки: `mkdir -p /data/test /data/node-red /data/influxdb /data/grafana`
   - назначить права:
   ```
   chown -R 472:root /data/grafana
   chown -R 1000:root /data/influxdb
   chmod 700 /data/influxdb
   chown -R 1000:1000 /data/node-red
   chown -R root:root /data/prometheus
   chown -R 31337:root /data/test
   ```
   - в файле `/etc/exports` прописать папки NFS:
   ```
   /data/test *(rw,no_root_squash,insecure,sync,subtree_check)
   /data/node-red *(rw,no_root_squash,insecure,async,no_subtree_check)
   /data/influxdb *(rw,no_root_squash,sync,no_subtree_check)
   /data/grafana *(rw,no_root_squash,sync,no_subtree_check)
   ```
   - обновить экспорты:
   ```  
   exportfs -ra
   systemctl enable rpc-statd
   service rpc-statd start
   ```

# Worker-ноды
1. Включить агента: `systemctl enable k3s-agent`
2. Проверка статуса агента Kubernetes:
   - `systemctl status k3s-agent`
   - `journalctl -u k3s-agent`

# Настройки

## Пред-настройка k8s
1. Скопировать файл `/etc/rancher/k3s/k3s.yaml` с мастер-ноды на свой хост в `~/.kube/config`
2. Настроить алиасы (на своём хосте): `source k8s/env` 
2. Проверить, что все ноды и поды заработали:
   - `kg no -o wide`
   - `kg po -A -o wide`
3. Пометить воркер-ноды:
   ```bash
   kg no -o wide
   k label no cardamom node-role.kubernetes.io/worker=worker
   k label no turmeric node-role.kubernetes.io/worker=worker
   ```

## Установка основных сервисов

### Helm-чарты
[Страница дистрибутивов Helm](https://github.com/helm/helm/releases)

```zsh
helm repo add stable https://charts.helm.sh/stable
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo add jetstack https://charts.jetstack.io
helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner
helm repo add influxdata https://helm.influxdata.com
helm repo update
```

helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add k8s-at-home https://k8s-at-home.com/charts/
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add kube-state-metrics https://kubernetes.github.io/kube-state-metrics

### MetalLB
```zsh
k create secret generic -n kube-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
ka metallb/config.yaml
ka metallb/metallb.yaml
```

### NGINX
```zsh
# helm install nginx ingress-nginx/ingress-nginx -n kube-system --set defaultBackend.enabled=false
helm install nginx ingress-nginx/ingress-nginx -n kube-system -f nginx/values.yaml
```
**Примечание:** external-адрес должен отвечать по http

### CertManager
```zsh
helm search repo jetstack
helm install cert-manager jetstack/cert-manager --namespace kube-system  --version v1.4.0 --set installCRDs=true
```

## Настройки кластера

### Добавление NFS для подов
[Документация](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner/blob/master/charts/nfs-subdir-external-provisioner/README.md)
```zsh
helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --set nfs.server=masala --set nfs.path=/data/test -n kube-system
ka storage/pv-nfs-20G.yaml
```

### Настройка сертификатов
Документация: 
[Самоподписанные](https://cert-manager.io/docs/configuration/selfsigned/),
[CA & Let's Encrypt](https://cert-manager.io/docs/configuration/ca/)

#### Самоподписанные
```zsh
ka cert-mgr/selfsigned-clusterissuer.yaml
```

#### Локальный CA
1. Сгенерировать самоподписанный CA
2. Закодировать ключ/сертификат в base64
   ```zsh
   cat ca.crt | base64 -b0  # на linux опция -w0
   cat ca.key | base64 -b0
   ```
3. Указать ключи в [ca-clusterissuer-secret.yaml](k8s/cert-mgr/ca-clusterissuer-secret.yaml) и установить CA:
   ```zsh
   ka cert-mgr/ca-clusterissuer-secret.yaml
   ka cert-mgr/ca-clusterissuer.yaml
   ```

#### Let's Encrypt
**Примечание:** _Для активации LE для домена используется DNS от AWS (Route53)_
1. В Route53 создать политику
   <details>
   <summary>Политика AWS Route53</summary>
   
   ```json
   {
     "Version": "2012-10-17",
     "Statement": [
       {
         "Effect": "Allow",
         "Action": "route53:GetChange",
         "Resource": "arn:aws:route53:::change/*"
       },
       {
         "Effect": "Allow",
         "Action": [
           "route53:ChangeResourceRecordSets",
           "route53:ListResourceRecordSets"
         ],
         "Resource": "arn:aws:route53:::hostedzone/Z05501933FETUZ44HZW0X"
       }
     ]
   }
   ```
   
   </details>
2. Создать в AWS пользователя _cert-manager_ и получить пару `accessKey/secretKey`
3. В файле [aws-route53-secret.yaml](k8s/cert-mgr/aws-route53-secret.yaml) указать `secretAccessKey`,
   в файлах [le-stg-clusterissuer.yaml](k8s/cert-mgr/le-stg-clusterissuer.yaml) и
   [le-prd-clusterissuer.yaml](k8s/cert-mgr/le-prd-clusterissuer.yaml) указать `accessKey`, применить настройки
   ```zsh
   ka cert-mgr/aws-route53-secret.yaml
   ka cert-mgr/le-stg-clusterissuer.yaml
   ka cert-mgr/le-prd-clusterissuer.yaml
   ```
4. Выписать тестовый сертификат (в Route53 создаётся тестовая запись с TTL=10сек)
   ```zsh
   ka cert-mgr/le-stg-cert-host.yaml
   ```
   **Примечание:** Если сертификат был выписан _LE Stage_, впоследствии его необходимо удалить, иначе 
   присутствующие в системе сертификат и секрет не дадут выписать сертификат с _LE Prod_ (будет выдавать ошибку).

## Проверка
После установки и настройки всех вышеперечисленных сервисов, запрос в _namespace_ _kube-system_ должен 
выводить следующее:
- `kg no -o wide`
   <details>
   <summary>Результат</summary>
   
   ```zsh
   NAME       STATUS   ROLES                  AGE   VERSION        INTERNAL-IP     EXTERNAL-IP   OS-IMAGE                       KERNEL-VERSION   CONTAINER-RUNTIME
   saffron    Ready    control-plane,master   20h   v1.21.2+k3s1   192.168.6.101   <none>        Debian GNU/Linux 10 (buster)   5.10.17-v8+      containerd://1.4.4-k3s2
   cardamom   Ready    worker                 16h   v1.21.2+k3s1   192.168.6.102   <none>        Debian GNU/Linux 10 (buster)   5.10.17-v8+      containerd://1.4.4-k3s2
   turmeric   Ready    worker                 16h   v1.21.2+k3s1   192.168.6.103   <none>        Debian GNU/Linux 10 (buster)   5.10.17-v8+      containerd://1.4.4-k3s2
   ```
   </details>

- `kg po -n kube-system -o wide`
   <details>
   <summary>Результат</summary>
   
   ```zsh
   NAME                                               READY   STATUS    RESTARTS   AGE     IP              NODE       NOMINATED NODE   READINESS GATES
   coredns-7448499f4d-q54kq                           1/1     Running   2          21h     10.42.1.8       saffron    <none>           <none>
   local-path-provisioner-5ff76fc89d-chtzw            1/1     Running   2          21h     10.42.1.9       saffron    <none>           <none>
   metrics-server-86cbb8457f-chb8x                    1/1     Running   2          21h     10.42.1.10      saffron    <none>           <none>
   metallb-speaker-rfpg7                              1/1     Running   0          3h30m   192.168.6.103   turmeric   <none>           <none>
   metallb-speaker-5gtfp                              1/1     Running   0          3h30m   192.168.6.101   saffron    <none>           <none>
   metallb-controller-7cf5df9947-fd9df                1/1     Running   0          3h30m   10.42.2.4       turmeric   <none>           <none>
   metallb-speaker-dvghp                              1/1     Running   0          3h30m   192.168.6.102   cardamom   <none>           <none>
   nginx-ingress-nginx-controller-6554479487-vwzq5    1/1     Running   0          104m    10.42.0.4       cardamom   <none>           <none>
   cert-manager-5d7f97b46d-r4xm8                      1/1     Running   0          90m     10.42.2.7       turmeric   <none>           <none>
   cert-manager-webhook-7b8b978f96-n8cpb              1/1     Running   0          90m     10.42.2.6       turmeric   <none>           <none>
   cert-manager-cainjector-69d885bf55-x8mkj           1/1     Running   0          90m     10.42.0.5       cardamom   <none>           <none>
   nfs-subdir-external-provisioner-7dbf8f4579-m6th4   1/1     Running   0          18m     10.42.0.6       cardamom   <none>           <none>
   ```
   </details>

- `kg svc -n kube-system -o wide`
   <details>
   <summary>Результат</summary>
   
   ```zsh
   NAME                                       TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)                      AGE    SELECTOR
   kube-dns                                   ClusterIP      10.43.0.10      <none>          53/UDP,53/TCP,9153/TCP       21h    k8s-app=kube-dns
   metrics-server                             ClusterIP      10.43.218.185   <none>          443/TCP                      21h    k8s-app=metrics-server
   nginx-ingress-nginx-controller-admission   ClusterIP      10.43.44.131    <none>          443/TCP                      104m   app.kubernetes.io/component=controller,app.kubernetes.io/instance=nginx,app.kubernetes.io/name=ingress-nginx
   nginx-ingress-nginx-controller             LoadBalancer   10.43.83.6      192.168.6.201   80:30489/TCP,443:30085/TCP   104m   app.kubernetes.io/component=controller,app.kubernetes.io/instance=nginx,app.kubernetes.io/name=ingress-nginx
   cert-manager                               ClusterIP      10.43.126.208   <none>          9402/TCP                     90m    app.kubernetes.io/component=controller,app.kubernetes.io/instance=cert-manager,app.kubernetes.io/name=cert-manager
   cert-manager-webhook                       ClusterIP      10.43.25.111    <none>          443/TCP                      90m    app.kubernetes.io/component=webhook,app.kubernetes.io/instance=cert-manager,app.kubernetes.io/name=webhook
   ```
   </details>

- `kg pv`
   <details>
   <summary>Результат</summary>
   
   ```zsh
   NAME       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
   nfs-data   20Gi       RWX            Retain           Available           nfs-client              18m
   ```
   </details>

- `kg storageclass`
   <details>
   <summary>Результат</summary>
   
   ```zsh
   NAME                   PROVISIONER                                     RECLAIMPOLICY   VOLUMEBINDINGMODE      ALLOWVOLUMEEXPANSION   AGE
   local-path (default)   rancher.io/local-path                           Delete          WaitForFirstConsumer   false                  21h
   nfs-client             cluster.local/nfs-subdir-external-provisioner   Delete          Immediate              true                   19m
   ```
   </details>

- `kg secret`
   <details>
   <summary>Результат</summary>
   
   ```zsh
   NAME                  TYPE                                  DATA   AGE
   default-token-kktsc   kubernetes.io/service-account-token   3      21h
   test-hous-link-tls    kubernetes.io/tls                     2      29m
   ```
   </details>

- `kg issuer`
   <details>
   <summary>Результат</summary>
   
   ```zsh
   No resources found
   ```
   </details>

- `kg clusterissuer`
   <details>
   <summary>Результат</summary>
   
   ```zsh
   NAME          READY   AGE
   internal-ca   True    59m
   le-staging    True    38m
   le-prod       True    38m
   ```
   </details>

- `kg cert`
   <details>
   <summary>Результат</summary>
   
   ```zsh
   NAME             READY   SECRET               AGE
   test-hous-link   True    test-hous-link-tls   32m
   ```
   </details>

- `kg ing`
   <details>
   <summary>Результат</summary>
   
   ```zsh
   
   ```
   </details>

# Установка приложений

## Mosquitto
1. Посмотреть настройки в mosquitto/config.yaml
2. Установить сервис: `ka mosquitto/`
3. Проверка
   <details>
   <summary>Результат успешного запуска (должен быт внешний IP-адрес полученный с LoadBalancer)</summary>
   
   ```zsh
   NAME                              READY   STATUS              RESTARTS   AGE
   mosquitto-8594777bc7-tc2mh        1/1     Running             0          21h
   NAME                   TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)          AGE
   mosquitto-service      LoadBalancer   10.43.81.216    192.168.6.202   1883:30409/TCP   21h
   ```

   </details>

## Node-RED
1. Проверить на сервере NFS:
    - создана папка данных для Node-RED 
    - установлены права 1000:1000
    - папка присутствует в `exportfs`
2. Установить Node-RED: `ka node-red/node-red.yaml`
3. Создать сервис Ingress: `ka node-red/ingress-ca.yaml`
4. Проверка `kg po && kg svc && kg ing`
   <details>
   <summary>Результат успешного запуска</summary>
   
   ```zsh
   NAME                        READY   STATUS    RESTARTS   AGE
   node-red-567497d89c-t8lrt   1/1     Running   0          3m22s
   NAME              TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
   kubernetes        ClusterIP   10.43.0.1     <none>        443/TCP   22h
   nodered-service   ClusterIP   10.43.104.1   <none>        80/TCP    3m23s
   NAME              CLASS    HOSTS     ADDRESS         PORTS     AGE
   nodered-ingress   <none>   nodered   192.168.6.201   80, 443   85s
   ```

   </details>
5. Зайти на веб-интерфейс по адресу https://nodered (добавить nodered в `/etc/hosts` или создать запись на DNS-сервере)
6. Установить пакеты:
    - node-red-contrib-alarm
    - node-red-contrib-arp
    - node-red-contrib-bool-gate
    - node-red-contrib-bravia
    - node-red-contrib-homekit-bridged
    - node-red-contrib-spruthub
    - node-red-contrib-stackhero-influxdb-v2
    - node-red-contrib-suncron
    - node-red-contrib-telegrambot
    - node-red-node-tail
    - node-red-contrib-zigbee2mqtt
    <details>
    <summary>Можно одной командой с шелла контейнера</summary>
    
    ```bash
    ke node-red-567497d89c-t8lrt  # имя пода/контейнера
    npm install --prefix /data node-red-contrib-alarm \
      node-red-contrib-arp \
      node-red-contrib-bool-gate \
      node-red-contrib-bravia \
      node-red-contrib-homekit-bridged \
      node-red-contrib-spruthub \
      node-red-contrib-stackhero-influxdb-v2 \
      node-red-contrib-suncron \
      node-red-contrib-telegrambot \
      node-red-node-tail \
      node-red-contrib-zigbee2mqtt
    reboot
    ```
   
    </details>

## InfluxDB
[Документация](https://github.com/influxdata/helm-charts)
### Настройки
1. Проверить на сервере NFS:
    - создана папка данных для InfluxDB 
    - установлены права 1000:0
    - папка присутствует в `exportfs`
2. Указать пароль и токен админа в файле [admin-auth.yaml](k8s/influxdb/admin-auth.yaml)
   **Примечание:** _Пароль и токен указываются в base64. Для кодирования использовать команду `base64 <<< password`.
   Для декодирования использовать команду `base64 -D <<< password`. Пароль по-умолчанию 'T0pSecret!',
   токен по-умолчанию '31337deadbeefdeadbeef31337'._
3. Если необходимо, поменять системные настройки в файле [influxdbv2.yaml](k8s/influxdb/influxdbv2.yaml), директивы
   - `DOCKER_INFLUXDB_INIT_ORG` - имя организации
   - `DOCKER_INFLUXDB_INIT_BUCKET` - системная корзина
4. Если необходимо, поменять имя хоста в файлах [influxdb-crt.yaml](k8s/influxdb/influxdb-crt.yaml) и 
   [ingress-ca.yaml](k8s/influxdb/ingress-ca.yaml)

### Установка
```zsh
ka influxdb/admin-auth.yaml
ka influxdb/influxdb-crt.yaml
ka influxdb/influxdb-pstor.yaml
ka influxdb/influxdbv2.yaml
ka influxdb/ingress-ca.yaml
```

Получить пароль админа можно командой: `echo $(kg secret influxdb-auth -o "jsonpath={.data['admin-password']}" | base64 -D)`

Проверка `kg po && kg svc && kg ing`
   <details>
   <summary>Результат успешного запуска</summary>
   
   ```zsh
   NAME                         READY   STATUS    RESTARTS   AGE
   influxdbv2-b5d7f84bf-dpw25   1/1     Running   0          11m
   NAME               TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
   influxdb-service   ClusterIP   10.43.139.131   <none>        80/TCP    11m
   NAME               CLASS    HOSTS      ADDRESS         PORTS     AGE
   influxdb-ingress   <none>   influxdb   192.168.6.201   80, 443   123m
   ```

   </details>

Зайти на веб-интерфейс по адресу [https://influxdb](https://influxdb) (добавить influxdb в `/etc/hosts` или создать запись на DNS-сервере)

# Бэкап

## Для сервера NFS
1. Установить клиента AWS S3 от MinIO: `wget https://dl.min.io/client/mc/release/linux-amd64/mc && install mc /usr/local/bin`
2. Добавить алиас для S3: `mc alias set s3 https://s3.amazonaws.com AKIA...W76 gpisOm...A38 --api S3v4`
3. Проверить, что работает: `mc cp test s3/spicy-backups/test`
4. Добавить в крон:

    ```sh
    #!/bin/sh

    DATA_FILE=/root/data-$(/bin/date +%Y%m%d%H%M).tar.gz
    tar --exclude='node_modules' --exclude='.npm' -zcf $DATA_FILE /data
    mc cp $DATA_FILE s3/spicy-backups/
    ```


# Дополнения

## Команды

### Мониторинг

#### Система
- `journalctl -fu k3s.service`
- `k3s journalctl logs`
- `free -h`
- `systemd-cgtop`

### Kubernetes

#### Системная информация
- `k cluster-info`
- `k top no`
- `k top po --all-namespaces`

#### Сертификаты
- `kg secret tls-secret -ojson` - посмотреть Secret-ы в формате json
- `kg secret … -ojson` - посмотреть сертификат 
- `kd cert ...name...` - получить описание сертификата 
- `kd csr` - получить описание запроса на серт
- `kg order [...name...]` - посмотреть Order-ы
- `kd challenges [...name...]` - посмотреть Challenge-ы 
- `kg issuers` - посмотреть Issuer-ов конкретного namespace
- `kg clusterissuers` - посмотреть Issuer-ов кластера

## Конфигурационные файлы

### Kubernetes K3S
- `/boot/dietpi.txt` - настройки при установке (см. директивы: _AUTO_SETUP_INSTALL_SOFTWARE_ID_, _SOFTWARE_K3S_EXEC_)
- `/boot/dietpi-k3s.yaml` - конфигурация k3s, копируется в _config.yaml_ при установке
- `/etc/rancher/k3s/config.yaml` - файл конфигурации мастер и воркер-нод
- `/etc/systemd/system/k3s.service` - конфигурация демона на мастер-ноде
  <details>
  <summary>Конфигурация с квотами</summary>
  
  ```editorconfig
  [Service]
  CPUQuota=30%
  CPUQuotaPeriodSec=50ms
  AllowedCPUs=0
  MemoryLimit=768M
  ```

  </details>
- `/etc/systemd/system/k3s-agent.service` - конфигурация демона на агенте
- `/var/lib/rancher/k3s/server/node-token` - токен для подсоединения к серверу (на мастере)
- `/etc/rancher/k3s/k3s.yaml` - конфигурация k8s для подкючения (секреты тут), копировать в ~/.kube/config

## Настройка и форматирование внешнего диска
1. Подготовка папки и поиск нужного диска
   ```bash
   mkdir /data
   lsblk
   fdisk /dev/sda
   o
   n
   w
   ```
2. Создать разделы
3. Отформатировать диск: `mkfs.ext4 /dev/sda1 -L data -E lazy_itable_init=0,lazy_journal_init=0`
4. Получить идентификатор диска: `blkid`
5. Создать запись в `/etc/fstab`
   ```
   PARTUUID=xxxx /data ext4 noatime,lazytime,rw,nofail,auto,x-systemd.automount
   ```
6. Примонтировать диск и поменять права:
   ```bash
   mount /data
   chown -R 1000:1000 /data
   chmod 770 /data
   ```

## Удаление невалидной ноды
```zsh
kg po -A
kdel po -n kube-system --force local-path-provisioner-xxx
kdel po -n kube-system --force coredns-xxx
kdel po -n kube-system --force metrics-server-xxx
k drain dietpi --ignore-daemonsets --delete-local-data
kdel no dietpi
```

## Установка kubectl на macOS
```zsh
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/darwin/amd64/kubectl"
sudo mv kubectl /usr/local/bin
mkdir -p ~/.kube
```

## Установка helm

### на macOS
```bash
curl https://get.helm.sh/helm-v3.6.2-darwin-amd64.tar.gz -o helm.tar.gz
tar -zxvf helm.tar.gz
sudo mv darwin-amd64/helm /usr/local/bin/helm
```

### на linux
```bash
curl https://get.helm.sh/helm-v3.6.2-linux-arm64.tar.gz -o helm.tar.gz
tar -zxvf helm.tar.gz
mv linux-arm64/helm /usr/local/bin/helm
```

## DPI
[Теория](https://habr.com/ru/post/548110/)

### Локальный сервер

в контейнере alpine:latest

```bash
kra
apk add git gcc perl make musl-dev pcre
cd /root
wget https://openresty.org/download/openresty-1.19.3.1.tar.gz
tar zxf openresty-1.19.3.1.tar.gz
git clone https://github.com/fffonion/lua-resty-openssl.git
git clone https://github.com/fffonion/lua-resty-openssl-aux-module.git
git clone https://github.com/Evengard/lua-resty-getorigdest-module.git
git clone https://github.com/iryont/lua-struct.git
git clone https://github.com/Evengard/lua-resty-socks5.git
cd openresty-1.19.3.1/
./configure --prefix=/opt/udpi --with-cc=gcc \
  --add-module=/root/lua-resty-openssl-aux-module \
  --add-module=/root/lua-resty-openssl-aux-module/stream \
  --add-module=/root/lua-resty-getorigdest-module/src
make -j4
```

### Удалённый сервер
SOCKS5 прокси-сервер для проксирования заблокированного трафика - например, 
socks-прокси TOR-а, либо обыкновенный ssh -D. Для VPN-сервера - надо установить 
его на самом VPN-сервере и проксировать через него
