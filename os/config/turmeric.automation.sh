#!/bin/sh

dpkg --add-architecture arm64
dpkg --remove-architecture armhf

apt clean
apt autoremove
apt update
apt full-upgrade
apt install iptables

mkdir -p /data
chown -R 1000:1000 /data

# mv /boot/k3s.sh /root
# chmod +x /root/k3s.sh

update-alternatives --set iptables /usr/sbin/iptables-legacy
update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy

systemctl enable k3s-agent
