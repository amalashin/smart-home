#!/bin/sh

dpkg --add-architecture arm64
dpkg --remove-architecture armhf

apt clean
apt autoremove
apt update
apt full-upgrade
apt install iptables

mkdir -p /data
chown -R 1000:1000 /data

update-alternatives --set iptables /usr/sbin/iptables-legacy
update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy

systemctl enable rpc-statd
service rpc-statd start
