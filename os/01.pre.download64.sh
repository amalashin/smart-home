#!/bin/zsh
# shellcheck shell=bash

mkdir -p ./tmp
cd ./tmp || exit

curl https://cdn.theunarchiver.com/downloads/unarMac.zip --output unar.zip
curl https://dietpi.com/downloads/images/DietPi_RPi-ARMv8-Buster.7z --output dietpi.7z

unzip unar.zip
./unar dietpi.7z
