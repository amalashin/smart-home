#!/bin/zsh
# shellcheck shell=bash

choice=no
dn=X
while [[ "$choice" != 'yes' ]]; do
  echo 'Select external disk number to eject'
  diskutil list | grep external
  read -r dn'?Disk number: '
  echo
  read -r choice"?Do you wish to eject /dev/disk$dn (yes|no)? "
done

rm -rf /Volumes/boot/.*
diskutil eject /dev/disk$dn

read -r response'?Remove temporary directory? [y/N] '
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
  rm -rf ./tmp
fi