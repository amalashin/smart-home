#!/bin/zsh
# shellcheck shell=bash

read -r response'?Do you have K3S Node Token? [y/N] '
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
    read -r TOKEN"?Enter Node token: "
    if [[ -z "${TOKEN// }" ]]
    then
      echo "Your token is empty. Please rerun this script and try again."
    else
      echo $TOKEN > /Volumes/boot/k3s_token
      cp ./config/cardamom.dietpi.txt /Volumes/boot/dietpi.txt
      cp ./config/cardamom.automation.sh /Volumes/boot/Automation_Custom_Script.sh
      cp ./config/cardamom.dietpi-k3s.yaml /Volumes/boot/dietpi-k3s.yaml
      echo boot_delay=15 >> /Volumes/boot/config.txt
    fi
else
    echo "I can't continue without a token"
    echo "Please get one from the master node"
    echo "With the command: cat /var/lib/rancher/k3s/server/node-token"
fi

#cp ./config/cardamom.dietpi.txt /Volumes/boot/dietpi.txt
#cp ./config/cardamom.automation.sh /Volumes/boot/Automation_Custom_Script.sh
#cp ./config/cardamom.dietpi-k3s.yaml /Volumes/boot/dietpi-k3s.yaml
# cp ./config/k3s.sh /Volumes/boot/

#echo boot_delay=15 >> /Volumes/boot/config.txt
