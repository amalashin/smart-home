#!/bin/zsh

cd ./tmp
diskutil unmountDisk /dev/disk$1
sleep 2
sudo dd if=./dietpi/DietPi_RPi-ARMv6-Buster.img of=/dev/rdisk$1 bs=1m
sleep 5

cd ..
cp ./config/cmdline32.txt /Volumes/boot/cmdline.txt
cp ./config/config32.txt /Volumes/boot/config.txt


cd /Volumes/boot
# rm -rf bootcode.bin kernel.img kernel7.img kernel7l.img bcm2708-rpi-b-plus.dtb bcm2708-rpi-b-rev1.dtb bcm2708-rpi-b.dtb bcm2708-rpi-cm.dtb bcm2708-rpi-zero-w.dtb bcm2708-rpi-zero.dtb bcm2709-rpi-2-b.dtb bcm2710-rpi-2-b.dtb bcm2710-rpi-3-b-plus.dtb bcm2710-rpi-3-b.dtb bcm2710-rpi-cm3.dtb bcm2711-rpi-cm4.dtb start.elf start4db.elf start_cd.elf start_db.elf start_x.elf fixup.dat fixup4db.dat fixup_cd.dat fixup_db.dat fixup_x.dat
rm -rf .Spotlight-V100 .fseventsd