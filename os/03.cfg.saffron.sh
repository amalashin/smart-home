#!/bin/zsh

cp ./config/saffron.dietpi.txt /Volumes/boot/dietpi.txt
cp ./config/saffron.automation.sh /Volumes/boot/Automation_Custom_Script.sh
cp ./config/saffron.dietpi-k3s.yaml /Volumes/boot/dietpi-k3s.yaml
