#!/bin/zsh
# shellcheck shell=bash

choice=no
dn=X
while [[ "$choice" != 'yes' ]]; do
  echo 'Select external disk number to write image to'
  diskutil list | grep external
  read -r dn'?Disk number: '
  echo
  read -r choice"?Do you wish to install the image onto /dev/disk$dn (yes|no)? "
done

cd ./tmp || exit
diskutil unmountDisk /dev/disk$dn
sleep 2
sudo dd if=./dietpi/DietPi_RPi-ARMv8-Buster.img of=/dev/rdisk$dn bs=1m
sleep 5

cd ..
cp ./config/cmdline.txt ./config/config.txt /Volumes/boot/


cd /Volumes/boot || exit
rm -rf bootcode.bin kernel.img kernel7.img kernel7l.img bcm2708-rpi-b-plus.dtb bcm2708-rpi-b-rev1.dtb bcm2708-rpi-b.dtb bcm2708-rpi-cm.dtb bcm2708-rpi-zero-w.dtb bcm2708-rpi-zero.dtb bcm2709-rpi-2-b.dtb bcm2710-rpi-2-b.dtb bcm2710-rpi-3-b-plus.dtb bcm2710-rpi-3-b.dtb bcm2710-rpi-cm3.dtb bcm2711-rpi-cm4.dtb start.elf start4db.elf start_cd.elf start_db.elf start_x.elf fixup.dat fixup4db.dat fixup_cd.dat fixup_db.dat fixup_x.dat
rm -rf .Spotlight-V100 .fseventsd
