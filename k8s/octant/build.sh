#!/bin/sh

VER=0.18.0
#ARCH=64bit
ARCH=arm64

mkdir -p ./tmp/build

curl -L https://github.com/vmware-tanzu/octant/releases/download/v${VER}/octant_${VER}_Linux-${ARCH}.tar.gz --output ./tmp/octant.tar.gz
tar -zxvf ./tmp/octant.tar.gz -C ./tmp

mv ./tmp/octant_${VER}_Linux-${ARCH}/octant ./tmp/build
cp kube.config ./tmp/build
cp Dockerfile ./tmp/build

vctl build -t amalashin/octant-${ARCH}:${VER}-1 ./tmp/build

rm -rf ./tmp
