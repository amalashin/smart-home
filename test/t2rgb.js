function clamp(x) {
    const min = 0;
    const max = 255;
    
    return x < min ? min : x > max ? max : Math.round(x);
}

function convert(kelvin) {
    const temp = kelvin / 100.0;
    var red, green, blue;
    
    if (temp < 66.0) {
        red = 255;
        
        // a = -155..., b = -0.445..., c = 104..., x = kelvin/100-2
        green = temp - 2.0;
        green = -155.25485562709179 - 0.44596950469579133 * green + 104.49216199393888 * Math.log(green);
        
        if (temp <= 20.0) { blue = 0 }
        else {
            // a = -254..., b = 0.827.., c = 155..., x = kelvin/100 - 10
            blue = temp - 10.0;
            blue = -254.76935184120902 + 0.8274096064007395 * blue + 115.67994401066147 * Math.log(blue);
        }
    } else {
        // a = 351..., b = 0.114..., c = -40..., x = kelvin/100-55
        red = temp - 55.0;
        red = 351.97690566805693 + 0.114206453784165 * red - 40.25366309332127 * Math.log(red);
        
        // a = 325..., b = 0.079..., c = -28..., x = kelvin/100-50
        green = temp - 50.0
        green = 325.4494125711974 + 0.07943456536662342 * green - 28.0852963507957 * Math.log(green);
        
        blue = 255;
    }
    
    return {
        r: clamp(red),
        g: clamp(green),
        b: clamp(blue)
    }
}

let rgb = convert(4470);
console.log(`rgb: ${JSON.stringify(rgb)}`);

process.exit(1);
