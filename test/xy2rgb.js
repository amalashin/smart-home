// xy + bri to rgb conversion

function scale(x) {
    return x < 0 ? 255 : Math.round(x);
}

function convert(x, y, bri){
    const z = 1.0 - x - y;
    const Y = bri * 100.0 / 254.0; // brightness of the lamp
    const X = (Y / y) * x;
    const Z = (Y / y) * z;
    
    let r = X * 1.612 - Y * 0.203 - Z * 0.302;
    let g = -X * 0.509 + Y * 1.412 + Z * 0.066;
    let b = X * 0.026 - Y * 0.072 + Z * 0.962;
    
    r = r <= 0.0031308 ? 12.92 * r : (1.0 + 0.055) * Math.pow(r, (1.0 / 2.4)) - 0.055;
    g = g <= 0.0031308 ? 12.92 * g : (1.0 + 0.055) * Math.pow(g, (1.0 / 2.4)) - 0.055;
    b = b <= 0.0031308 ? 12.92 * b : (1.0 + 0.055) * Math.pow(b, (1.0 / 2.4)) - 0.055;
    
    let max = Math.max(r, g, b);
    
    r /= max;
    g /= max;
    b /= max;

    return { r: scale(r * 255), g: scale(g * 255), b: scale(b * 255) }
}

console.log(convert(0.5724, 0.3581, 99));
console.log(convert(0.5756, 0.3270, 99));
