function clamp(x) {
    const min = 153;
    const max = 500;
    
    return x < min ? min : x > max ? max : x;
}

function convert(mired) {
    const K = 1000000 / clamp(mired);
    
    return Math.round(K);
}

console.log(convert(153));
console.log(convert(500));
console.log(convert(447));
console.log(convert(200));